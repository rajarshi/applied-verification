import onnx
import onnxruntime
import torch
import numpy as np
from autoencoder import *
from maraboupy import Marabou
from maraboupy import MarabouCore




print("----------Loading autoencoder----------")
architecture=[10,5,10]

aut = autoencoder(architecture=architecture)
aut.loadAE('trained_autoencoders/sine_curve')

dummy_input = torch.randn(10)

print("----------Exporting to onnx----------")	
torch.onnx.export(aut.module, dummy_input, "autoencoder.onnx", export_params=True, output_names=["output1", "output2"])

print("----------Reading onnx file----------")
ort_session = onnxruntime.InferenceSession("autoencoder.onnx")

def to_numpy(tensor):
	return tensor.detach().cpu().numpy() if tensor.requires_grad else tensor.cpu().numpy()

ort_inputs = {ort_session.get_inputs()[0].name: to_numpy(dummy_input)}
ort_outs = ort_session.run(None, ort_inputs)

print("----------Providing constraints----------")

network = Marabou.read_onnx("autoencoder.onnx", outputName="output2")


oVars = network.outputVars
iVars = network.inputVars[0]

for i in range(architecture[0]):

	#Constraints for bounds for network variables
	network.setLowerBound(iVars[i], -1)
	network.setUpperBound(iVars[i], 1)
	network.setLowerBound(oVars[i], -1)
	network.setUpperBound(oVars[i], 1)

	#Constraints for adversarial example 
	eq1 = MarabouCore.Equation(MarabouCore.Equation.GE)
	eq1.addAddend(1, iVars[i])
	eq1.addAddend(-1, oVars[i])
	eq1.setScalar(0.1)

	eq2 = MarabouCore.Equation(MarabouCore.Equation.GE)
	eq2.addAddend(1, oVars[i])
	eq2.addAddend(-1, iVars[i])
	eq2.setScalar(0.1)

	network.addDisjunctionConstraint([[eq1],[eq2]])


network.solve()





