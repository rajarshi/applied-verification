How to use Planet for verifying Autoencoder:
----------------------------------

For installing [Planet](https://github.com/progirep/planet) follow the installation directives provided in the github link of the tool.
You can use the shell command `export PATH=path/to/planet/src` to add Planet to your list of shell directories.

The constraints for Planet have to be provided in a rlv file format. To run a rlv file using Planet write the shell command `planet ae_example.rlv`, where 'ae_example.rlv' is the rlv file provided the repository.

For writing a constraint in rlv file, for example `2*x0 <= 3` in a file `file.rlv`, you need to simply write the following in your python code:

	f.open("file.rlv", "a")
	f.write("Assert <= 3.0 2.0 x0")

Further, to write disjunctions, you need to use ReLU variables to mimic the process. For instance, to write `2*x0 < 3 or 4*x1 < 5`, you have to add the following constraints to your rlv file:

	ReLU r1 3.0 -2.0 x0
	ReLU r2 5.0 -4.0 x1
	Linear l 1.0 r1 1.0 r2
	Assert < 0.0 1.0 l

In the above set of constraints, we assert variable l to be greater than 0, that can occur only when at least one of the variables r1 or r2 has a value greater than 0.  

Finally, for using Planet from python the add the following to your python command (requires to import subprocess package in the code):
	
	subprocess.run(['planet', 'file.rlv'], stdout=subprocess.PIPE)










