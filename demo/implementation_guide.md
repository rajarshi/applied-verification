# Verification of Autoencoders


We provide a guide to the implementation of verification of various properties of autoencoders. The implementation we describe is based on this [paper](http://ls9-www.cs.tu-dortmund.de/publications/ECMLPKDD2020.pdf). Before we proceed to provide the details of the implementation, we briefly introduce the theoretical concepts used in the implementation.

Autoencoder is a feed-forward neural network with a special architecture (arrangement of neurons). In particular, the number of neurons in input layer is equal to the number of neurons in the output layer. Additionally, autoencoders have a hidden layer, separating the input and the output layers, which has lesser number of neurons than the input layer. This layer produces an information bottleneck making autoencoders well suited for dimensionality reduction and denoising of data. 

Now for describing properties of interest of autoencoder, we interpret an autoencoder as a function f: R^n^ -> R^n^. In order to make the problem of verfying autoencoders computationally tractable,  we fix a region r in the input space which is a subset of R^n^. We check the following three properties of an autoencoder in the region r: 

- *Existence of adversarial examples*: An epsilon-adversarial example of an autoencoder f is a point x such that dist(x,f(x))>epsilon. Intuitively, an adversarial example is a point on which the input and output of an autoencoder deviate more than epsilon. 
  
- *Adversarial robustness*: An autoencoder is (delta, epsilon)-adversarially robustness at a given point x in region r, if for all points y in r, if dist(x,y) < delta then dist(f(x), f(y)) < epsilon

- *Fairness*: An autoencoder is epsilon-fair in region r, if for all points x,y in R if x and y do not match in exactly one dimension, then, dist(x,y) < epsilon. 

We now proceed to describe the implementation.

### How to use:

We provide a simple demonstration of how to implement various verification algorithms for autoencoders in python3. We also provide instructions of how to use it on Linux operating system. The implementation uses various python packages. Thus, in order to install it on a local machine, we recommend using a python virtual environment. You can create a python virtual environment using the following shell command

``virtualenv -p python3 venv``

You can activate the virtual environment `venv` using the following command:

``source venv/bin/activate``

After activating your virtual environment, you can install the dependencies required for the implementation using the following:

``pip install -r requirements.txt``


### Details of the implementation:
To run the program you would need to simply run the following command `python run-test.py` from the folder `demo`. This program simply trains an autoencoder of architecture (10,5,10) on dataset `datasets/sine-curve.csv` and checks for 0.1-adversarial examples in the unit square around origin. We describe the various components of the code in order to have a better understanding of the implementation.


##### Implementing Autoencoders
We implement autoencoders using a popular python package called [PyTorch](https://pytorch.org/) used extensively for implementing neural networks. The python program `autoencoder.py` provided in the demo stub, displays how to implement an autoencoder using the pytorch package.
The class `autoencoder` defines an autoencoder, which can be initialized with various parameters such as architecture, activation function and initial values of the autoencoder. The important methods of this class are listed belows:

- `train(data, epochs, batch_size, learning_rate)`: which trains autoencoders on given *data*---it can be provided with parameters such as number of *epochs*, *batch size* and *learning rate* required for the training process.
- `predict(data)`: computes the output of an autoencoder on given *data*.
- `saveAE(folder)`: stores the parameters of a trained autoencoder in the specified *folder*.
- `loadAE(folder)`: loads a saved autoencoder from the specified *folder*.

The folder `trained-autoencoders` contains few trained networks for performing various experiments.

##### About the data

The data used for training autoencoders is available in the folder `datasets`, which consists of three datasets: ECG data (taken from [http://www.timeseriesclassification.com/](timeseriesclassification.com)), noisy sine-curve and noisy straight line. The latter two datasets are synthetically generated using functions in the python program `data_generator.py`. This python program also contains a function `plot_output(datasets)` which produces a graph for visualization of the data in *datasets* (using matplotlib package).


##### Checking properties

For checking properties of autoencoders, we use an approach of encoding the properties as satisfiability problems in quantifier-free [first order formula](https://en.wikipedia.org/wiki/First-order_logic) over real arithmetic. For providing such encodings we use a popular SMT solver called [Z3](https://github.com/Z3Prover/z3). For an easy introduction to Z3 API in python, you can use [this](ericpony.github.io/z3py-tutorial/guide-examples.htm). We have provided the encoding in the program `encoding.py`. The most important method of this class `SMTencoding` is 

- `checkProperties(autoencoder, prop, boundingBox)`: takes as input an autoencoder, a property given as a list *prop* consisting various parameters required for checking the property and a region defined by *boundingBox*; it returns counterexample if the property is violated, returns *None* otherwise.


### Neural Network Verifiers

Recently, there has been developmement of several neural network verification tools. Tools such as [Planet](https://github.com/progirep/planet) and [Reluplex](https://github.com/guykatzz/ReluplexCav2017) are solvers that allow checking of various properties of neural networks.
For installing the tools follow the installation directives provided in the github link of the tools.


