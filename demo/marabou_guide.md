How to use Marabou for verifying Autoencoder:
----------------------------------

The verifier [Marabou](https://github.com/NeuralNetworkVerification/Marabou) is now setup in the virtual machine used for the project. We provide a python program [`convertor.py`](https://gitlab.mpi-sws.org/rajarshi/applied-verification/-/blob/master/demo/convertor.py) to demonstrate how to use Marabou to search for adversarial examples in a network. The following set of shell commands will enable you to run the program.


	cd ~rajarshi/applied-verification/demo
	source venv3.5/bin/activate
	export PYTHONPATH=~rajarshi/Marabou/
	python convertor.py

For more information on how to use the Marabou API for Python3, please follow [this](https://neuralnetworkverification.github.io/Marabou/) documentation.








